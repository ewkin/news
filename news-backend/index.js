const express = require('express');
const cors = require("cors");
const fileDb = require('./fileDb');
const commentDb = require('./commentDb')
const news = require('./app/news');
const comment = require('./app/comments');

const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

const port = 8000;

app.use('/news', news);
app.use('/comment', comment);

const run = async () => {
    await fileDb.init();
    await commentDb.init();
    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
}
run().catch(console.error);