const fs = require('fs').promises;
const {nanoid} = require('nanoid');
const filename = './db.json';

let data = [];

module.exports = {
    async init() {
        try {
            const fileContent = await fs.readFile(filename);
            data = JSON.parse(fileContent);
        } catch (e) {
            data = [];
        }
    },
    async getItems() {
        return data;
    },
    async deleteItem(id) {
        data.splice(data.findIndex(function (i) {
            return i.id === id;
        }), 1);
        return data;
    },
    async getItemById(id) {
        let item = data.find(item => item.id === id);
        if(item){
            return data.find(item => item.id === id);
        } else {
            return undefined;
        }

    },
   async addItem(item) {
        item.id = nanoid();
       item.datetime = new Date();
        data.push(item);
        await this.save();
    },
    async save() {
        await fs.writeFile(filename, JSON.stringify(data, null, 2));
    }
};
