const path = require('path');
const express = require('express');
const multer = require('multer');
const fileDb = require('../fileDb');
const commDb = require('../commentDb');
const {nanoid} = require('nanoid');
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath)
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    const category = await fileDb.getItems();
    res.send(category);
});

router.delete('/:id', async (req, res)=>{
    await commDb.cascadeDelete(req.params.id);
    await fileDb.deleteItem(req.params.id);
    res.send('Got a DELETE request at '+req.params.id);
});

router.get('/:id', async (req, res) => {
    const category = await fileDb.getItemById(req.params.id);
    if(category){
        res.send(category);
    } else {
        console.log(category);
        return res.status(400).send({"error": 'incorrect URL'});
    }
});


router.post('/', upload.single('image'), async (req, res) => {
    const threads = req.body;
    if (req.file) {
        threads.image = req.file.filename;
    }
    if (threads.text && threads.name) {
        await fileDb.addItem(threads);
        res.send(threads);

    } else {
        return res.status(400).send({"error": 'Text must be present in the request'});
    }


});

module.exports = router;