const express = require('express');
const fileDb = require('../commentDb');
const newsDb = require('../fileDb')


const router = express.Router();

router.get('/', async (req, res) => {
    const message = await fileDb.getItemById(req.query.id);
    res.send(message);
});

router.delete('/:id', async (req, res) => {
    await fileDb.deleteItem(req.params.id)
    res.send('Got a DELETE request at ' + req.params.id);
});

router.post('/', async (req, res) => {
    const comment = req.body;
    if (comment.comment && await newsDb.getItemById(comment.idNews)) {
        await fileDb.addItem(comment);
        res.send(comment);
    } else {
        return res.status(400).send({"error": 'Text amd id must be present in the request'});
    }


});

module.exports = router;