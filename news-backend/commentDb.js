const fs = require('fs').promises;
const {nanoid} = require('nanoid');
const filename = './commentdb.json';

let data = [];

module.exports = {
    async init() {
        try {
            const fileContent = await fs.readFile(filename);
            data = JSON.parse(fileContent);
        } catch (e) {
            data = [];
        }
    },
    async getItems() {
        return data;
    },
    async deleteItem(id) {
        data.splice(data.findIndex( (i) => {
            return i.id === id;
        }), 1);
        return data;
    },
    async cascadeDelete(id) {
        data = data.filter(comment => !(comment.idNews===id))
        //
        // data.splice(data.findIndex( (i) => {
        //     return i.idNews === id;
        // }), 1);
        return data;
    },
    async getItemById(id) {
        let item = data.filter((value) => {
            return value.idNews === id;
        })
        return item;


    },
    async addItem(item) {
        item.id = nanoid();
        data.push(item);
        await this.save();
    },
    async save() {
        await fs.writeFile(filename, JSON.stringify(data, null, 2));
    }
};
