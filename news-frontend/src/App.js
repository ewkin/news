import React from 'react';
import CssBaseline from "@material-ui/core/CssBaseline";
import {Route, Switch} from "react-router-dom";
import AppToolBar from "./components/UI/AppToolBar/AppToolBar";
import Container from "@material-ui/core/Container";
import NewsPage from "./containers/NewsPage";
import OneNewsPage from "./containers/OneNewsPage";

const App = () => (
    <>
        <CssBaseline/>
        <header><AppToolBar/></header>
        <main>
            <Container maxWidth="xl">
                <Switch>
                    <Route path="/" exact component={NewsPage}/>
                    <Route path="/news/:id" component={OneNewsPage}/>
                    <Route render={() => <h1>Not found</h1>}/>
                </Switch>
            </Container>
        </main>
    </>
);

export default App;
