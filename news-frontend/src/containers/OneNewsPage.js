import React, {useEffect, useState} from 'react';
import axiosApi from "../axiosApi";
import {Card, CardContent, CardMedia} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import {apiURL} from "../config";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import TextField from "@material-ui/core/TextField";
import moment from "moment";
import {createComments, deleteComment, fetchComments} from "../store/actions/commentsActions";
import {useDispatch, useSelector} from "react-redux";
import Comment from "../components/Comment/Comment";

const useStyles = makeStyles({
    root: {
        maxWidth: '100%',
        marginBottom: '10px',
        padding: 20
    },
    media: {
        width: '40%',
        height: 100,
        backgroundSize: "contain"

    },
});

const OneNewsPage = props => {

    const dispatch = useDispatch();
    const classes = useStyles();
    const comments = useSelector(state => state.comments.comment);
    const [news, setNews] = useState({});
    const [comment, setComment] = useState({
        name: '',
        comment: '',
        idNews: props.match.params.id,
    });

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get('/news/' + props.match.params.id);
            setNews(response.data);
            dispatch(fetchComments(props.match.params.id));
        }
        fetchData().catch(console.error);
    }, [props.match.params.id, dispatch]);

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setComment(prevState => ({
            ...prevState, [name]: value
        }));
    };

    const submitFormHandler = async e => {
        e.preventDefault();
        await dispatch(createComments(comment));
        setComment({name: '', comment: '', idNews: props.match.params.id})
    };
    const toDelete = (id) => {
        dispatch(deleteComment(id, props.match.params.id));
    };

    return (
        <>
            <Card className={classes.root}>
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {news.name}
                    </Typography>
                    <Typography gvariant="body2" color="textSecondary" component="p">
                        {moment(news.datetime).format('MMMM Do YYYY, h:mm a')}
                    </Typography>
                </CardContent>
                {news.image ? (<CardMedia
                    className={classes.media}
                    image={apiURL + '/uploads/' + news.image}
                />) : null}
                <CardContent>
                    <Grid item container direction="row" justify="space-between" alignItems="center">
                        <Grid item>
                            <Typography variant="body2" color="textSecondary" component="p">
                                {news.text}
                            </Typography>
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
            {comments.map(comm => (
                <Comment
                    key={comm.id}
                    name={comm.name}
                    comment={comm.comment}
                    toDelete={() => toDelete(comm.id)}
                />

            ))}
            <Card className={classes.root}>
                <form onSubmit={submitFormHandler}>
                    <Grid container direction="column" spacing={2}>
                        <Grid item xs>
                            <TextField
                                fullWidth
                                variant="outlined"
                                id="name"
                                label="Name"
                                name="name"
                                value={comment.name}
                                onChange={inputChangeHandler}/>
                        </Grid>
                        <Grid item xs>
                            <TextField
                                fullWidth
                                multiline
                                rows={3}
                                required
                                variant="outlined"
                                id="comment"
                                label="comment"
                                name="comment"
                                value={comment.comment}
                                onChange={inputChangeHandler}/>
                        </Grid>
                        <Grid item xs>
                            <Button type="submit" color="primary" variant="contained">
                                Add
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            </Card>
        </>
    );
};

export default OneNewsPage;