import React, {useEffect} from 'react';
import OneNews from "../components/OneNews/OneNews";
import {Backdrop, CircularProgress, Fade, Modal} from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";
import ThreadForm from "../components/ThreadForm/ThreadForm";
import {useDispatch, useSelector} from "react-redux";
import {createNews, deleteNews, fetchNews, showModal} from "../store/actions/newsActions";
import Grid from "@material-ui/core/Grid";


const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}));


const NewsPage = props => {

        const classes = useStyles();
        const dispatch = useDispatch();
        const open = useSelector(state => state.news.showModal);
        const news = useSelector(state => state.news.news);
        const loading = useSelector(state => state.news.newsLoading);

        useEffect(() => {
            dispatch(fetchNews());
        }, [dispatch]);

        const onFormSubmit = async newsData => {
            await dispatch(createNews(newsData));
        };

        const readMore = async id => {
            props.history.push('/news/'+id);
        };

        const onDelete = async id => {
            dispatch(deleteNews(id));
        };


        const handleClose = () => {
            dispatch(showModal(false));
        };


        return (
            <>
                {loading ? (
                    <Grid container justify='center' alignContent="center" className={classes.progress}>
                        <Grid item>
                            <CircularProgress/>
                        </Grid>
                    </Grid>) : news.map(news => (
                    <OneNews
                        key={news.id}
                        id={news.id}
                        name={news.name}
                        dateOfNews={news.datetime}
                        readMore={() => readMore(news.id)}
                        onDelete={() => onDelete(news.id)}
                        text={news.text}
                        image={news.image}
                    />
                ))}
                <Modal
                    aria-labelledby="spring-modal-title"
                    aria-describedby="spring-modal-description"
                    className={classes.modal}
                    open={open}
                    onClose={handleClose}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                        timeout: 500,
                    }}
                >
                    <Fade in={open}>
                        <div className={classes.paper}>
                            <ThreadForm onSubmit={onFormSubmit}/>
                        </div>
                    </Fade>
                </Modal>
            </>
        )
            ;
    }
;

export default NewsPage;