import React from 'react';
import moment from 'moment';
import {Card, CardContent, CardMedia} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {apiURL} from "../../config";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles({
    root: {
        maxWidth: '100%',
        marginBottom: '10px',
        padding :20
    },
    media: {
        width: '40%',
        height: 100,
        backgroundSize: "contain"

    },
});


const OneNews = ({name, dateOfNews, text, image, onDelete, readMore}) => {
    const classes = useStyles();

    const trimText = (text, limit) => {
        text = text.trim();
        if (text.length <= limit) return text;

        text = text.slice(0, limit);
        const lastSpace = text.lastIndexOf(" ");

        if (lastSpace > 0) {
            text = text.substr(0, lastSpace);
        }
        return text + "...";
    }


    return (
        <Card className={classes.root}>
            <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                    {name}
                </Typography>
            </CardContent>
            {image ? (<CardMedia
                className={classes.media}
                image={apiURL + '/uploads/' + image}
            />) : null}
            <CardContent>
                <Grid item container direction="row" justify="space-between" alignItems="center">
                    <Grid item>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {trimText(text, 100)}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {moment(dateOfNews).format('MMMM Do YYYY, h:mm a')}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Button onClick={readMore}>Read More</Button>
                        <Button onClick={onDelete}>Delete</Button>
                    </Grid>
                </Grid>


            </CardContent>
        </Card>
    );
};

export default OneNews;