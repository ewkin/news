import React from 'react';
import {Card, CardContent} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles({
    root: {
        maxWidth: '95%',
        marginRight:'5%',
        marginBottom: '10px',
        padding: 20
    },
});



const Comment = ({name, comment, toDelete}) => {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                    {name? name : 'Anonymous'}
                </Typography>
            </CardContent>
            <CardContent>
                <Grid item container direction="row" justify="space-between" alignItems="center">
                    <Grid item>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {comment}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Button onClick={toDelete}>Delete</Button>
                    </Grid>
                </Grid>
            </CardContent>
        </Card>
    );
};

export default Comment;