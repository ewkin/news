import axiosApi from "../../axiosApi";
import {NotificationManager} from 'react-notifications';

export const FETCH_COMMENTS_REQUEST = 'FETCH_COMMENTS_REQUEST';
export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const FETCH_COMMENTS_FAILURE = 'FETCH_COMMENTS_FAILURE';
export const SHOW_MODAL = 'SHOW_MODAL';

export const CREATE_COMMENTS_SUCCESS = 'CREATE_COMMENTS_SUCCESS';

export const fetchCommentsRequest = () => ({type: FETCH_COMMENTS_REQUEST});
export const fetchCommentsSuccess = comment => ({type: FETCH_COMMENTS_SUCCESS, comment});
export const fetchCommentsFailure = () => ({type: FETCH_COMMENTS_FAILURE});

export const showModal = action => ({type: SHOW_MODAL, action});

export const createCommentsSuccess = () => ({type: CREATE_COMMENTS_SUCCESS});

export const fetchComments = (id) => {
    return async dispatch => {
        try {
            dispatch(fetchCommentsRequest());
            const response = await axiosApi.get('/comment?id=' + id);
            dispatch(fetchCommentsSuccess(response.data));
        } catch (e) {
            dispatch(fetchCommentsFailure());
            NotificationManager.error('Could not fetch threads')
        }
    }
};

export const deleteComment = (id, idNews) => {
    return async dispatch => {
        try {
            await axiosApi.delete('/comment/' + id);
            dispatch(fetchComments(idNews));
        } catch (e) {
            NotificationManager.error('Could not fetch threads')
        }
    }
}
export const createComments = commentData => {
    return async dispatch => {
        try {
            await axiosApi.post('/comment', commentData);
            dispatch (fetchComments(commentData.idNews));
        } catch (e) {
            NotificationManager.error('Could not post your news');
            dispatch(fetchCommentsFailure());
        }

    };
};