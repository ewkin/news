import axiosApi from "../../axiosApi";
import {NotificationManager} from 'react-notifications';

export const FETCH_NEWS_REQUEST = 'FETCH_NEWS_REQUEST';
export const FETCH_NEWS_SUCCESS = 'FETCH_NEWS_SUCCESS';
export const FETCH_NEWS_FAILURE = 'FETCH_NEWS_FAILURE';
export const SHOW_MODAL = 'SHOW_MODAL';

export const CREATE_NEWS_SUCCESS = 'CREATE_NEWS_SUCCESS';

export const fetchNewsRequest = () => ({type: FETCH_NEWS_REQUEST});
export const fetchNewsSuccess = news => ({type: FETCH_NEWS_SUCCESS, news});
export const fetchNewsFailure = () => ({type: FETCH_NEWS_FAILURE});

export const showModal = action => ({type: SHOW_MODAL, action});

export const createNewsSuccess = () => ({type: CREATE_NEWS_SUCCESS});

export const fetchNews = () => {
    return async dispatch => {
        try {
            dispatch(fetchNewsRequest());
            const response = await axiosApi.get('/news');
            dispatch(fetchNewsSuccess(response.data));
        } catch (e) {
            dispatch(fetchNewsFailure());
            NotificationManager.error('Could not fetch threads')
        }
    }
};

export const deleteNews = id => {
    return async dispatch => {
        try {
            await axiosApi.delete('/news/' + id);
            dispatch(fetchNews());
        } catch (e) {
            NotificationManager.error('Could not fetch threads')
        }
    }
}
export const createNews = newsData => {
    return async dispatch => {
        try {
            await axiosApi.post('/news', newsData);
            dispatch(fetchNews());
            dispatch(showModal(false));
            dispatch(createNewsSuccess());
        } catch (e) {
            NotificationManager.error('Could not post your news');
            dispatch(fetchNewsFailure());
        }

    };
};