import {FETCH_NEWS_FAILURE, FETCH_NEWS_REQUEST, FETCH_NEWS_SUCCESS, SHOW_MODAL} from "../actions/newsActions";

const initialState = {
    news: [],
    newsLoading: false,
    showModal: false,
};

const newsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_NEWS_REQUEST:
            return {...state, threadLoading: true};
        case FETCH_NEWS_SUCCESS:
            return {...state, news: action.news, newsLoading: false};
        case FETCH_NEWS_FAILURE:
            return {...state, threadLoading: false};
        case SHOW_MODAL:
            return {...state, showModal: action.action};
        default:
            return state;
    }
};

export default newsReducer;