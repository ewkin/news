import {SHOW_MODAL} from "../actions/newsActions";
import {FETCH_COMMENTS_FAILURE, FETCH_COMMENTS_REQUEST, FETCH_COMMENTS_SUCCESS} from "../actions/commentsActions";

const initialState = {
    comment: [],
    threadLoading: false,
    showModal: false,
};

const commentReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENTS_REQUEST:
            return {...state, threadLoading: true};
        case FETCH_COMMENTS_SUCCESS:
            return {...state, comment: action.comment, threadLoading: false};
        case FETCH_COMMENTS_FAILURE:
            return {...state, threadLoading: false};
        case SHOW_MODAL:
            return {...state, showModal: action.action};
        default:
            return state;
    }
};

export default commentReducer;